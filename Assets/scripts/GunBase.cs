using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GunBase : MonoBehaviour
{
    [SerializeField]
    public Transform GunPoint;
    public GameObject Gun;

    public Animator _myAnimation;

    [SerializeField]
    [Range(3, 200)]
    public float shootdistance;
    [SerializeField]
    [Range(0.25f, 2)]
    public float firerate;
    [SerializeField]
    [Range(1, 30)]
    public int gunDamage;
    



    public abstract void Shoot(bool isPlayer, GameObject player);    



}
