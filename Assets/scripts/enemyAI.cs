using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class enemyAI : MonoBehaviour
{
    [Header("Go's")]
    [SerializeField]    
    private NavMeshAgent _agent;
    private GameObject _player;
    [SerializeField]
    private Transform body;
    [SerializeField]  
    private GunBase _gun;
    [SerializeField]
    private GameObject exclemationmark;
    

    [Header("Layermasks")]
    [SerializeField]
    private LayerMask _whatIsGround, _whatIsPlayer;


    [Header("Attacking")]
    public bool IsAgressive = true;
    [Range(1, 5)]
    public int EnemyDamage = 2;
    [SerializeField]
    [Range(5, 50)]
    private float _sightRange;
    private bool _playerInSightRange;
    public bool justattacked = false;

    private float _shoottimer = 0;

    [Header("Patrolling")]
    private Vector3 _walkPoint;
    private Vector3 _startPatrollpoint;
    private bool _walkPointSet;
    [SerializeField]
    [Range(1, 30)]
    private float _walkPointRange;
    [SerializeField]
    [Range(1, 20)]
    private float lookrotationspeed;


    [Header("health")]
    public int health;

    [Header("colourchange")]
    private bool turnRed = false;
    private float turnredTimer = 0;
    [SerializeField]
    private MeshRenderer _myMeshRenderer;
    [SerializeField]
    private Material _hitMaterial;
    private Material _defaultmaterial;

    [Header("Voice Automated")]
    [SerializeField]
    private bool automatevoice = false;
    [SerializeField]
    [Range(0, 60)]
    private float _minvoicelineTimer;
    [SerializeField]
    [Range(0, 120)]
    private float _maxVoicelineTimer;
    private float _voiceSwitchTime;
    private float _enemyvoicelinetimer;

    private AudioSource _myaudiosource;

    [Header("Voice clips")]
    [SerializeField]
    private List<AudioClip> EnemyVoiceLines = new List<AudioClip>();



    private void Awake()
    {
        _player = GameObject.Find("Player");
        _agent = GetComponent<NavMeshAgent>();
        _defaultmaterial = _myMeshRenderer.material;
        _startPatrollpoint = transform.position;
        _myaudiosource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        Attacktimer();

        RedmaterialTurnBack();

        Statechecks();

        DeathTrigger();

        if (automatevoice)
        {
            playrandomvoice();
        }
    }

    private void playrandomvoice()
    {
        _enemyvoicelinetimer += Time.deltaTime;

        if (_voiceSwitchTime < _enemyvoicelinetimer)
        {
            _enemyvoicelinetimer = 0;
            _voiceSwitchTime = Random.Range(_minvoicelineTimer, _maxVoicelineTimer);

            AudioClip cliptoplay = EnemyVoiceLines[Random.Range(0, EnemyVoiceLines.Count - 1)];
            _myaudiosource.PlayOneShot(cliptoplay);
        }
    }

    public void PlayRandomSound()
    {
        AudioClip cliptoplay = EnemyVoiceLines[Random.Range(0, EnemyVoiceLines.Count - 1)];
        _myaudiosource.PlayOneShot(cliptoplay);
    }

    public void ChangePatrollCenterPoint()
    {
        _startPatrollpoint = transform.position;
    }

    private void DeathTrigger()
    {
        if (health <= 0)
        {
            EnemyDeath();
        }
    }

    private void RedmaterialTurnBack()
    {
        if (turnRed)
        {
            turnredTimer += Time.deltaTime;
            if (turnredTimer > 0.3f)
            {
                _myMeshRenderer.material = _defaultmaterial;
                turnredTimer = 0;
                turnRed = false;
            }
        }
    }

    private void Statechecks()
    {
        _playerInSightRange = Physics.CheckSphere(transform.position, _sightRange, _whatIsPlayer);


        if (!_playerInSightRange)
        {
            Patroling();
            exclemationmark.SetActive(false);
        }
        else
            exclemationmark.SetActive(true);

        if (_playerInSightRange && !justattacked && !LineOfSightCheck())
        {
            ChasePlayer();            
        }

        if (_playerInSightRange && !justattacked && LineOfSightCheck())
        {
            Vector3 lookPos = _player.transform.position - _gun.transform.position;
            lookPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, lookrotationspeed * Time.deltaTime);
                       
        }

        if (_playerInSightRange && !justattacked && LineOfSightCheck() && facingtarget())
        {
            if (IsAgressive)
            {
                Shoot();
            }

        }

        if (_playerInSightRange && justattacked)
        {
            StandYourGround();
        }

    }

    private void Attacktimer()
    {
        if (justattacked)
        {
            _shoottimer += Time.deltaTime;
            if (_shoottimer > _gun.firerate)
            {
                justattacked = false;
                _shoottimer = 0;
            }
        }
    }

    private void Shoot()
    {
        _gun.Shoot(false, _player);
        justattacked = true;
    }

    private bool facingtarget()
    {      

        RaycastHit hit;
        if (Physics.Raycast(transform.position, body.transform.forward, out hit, _sightRange + 5, LayerMask.GetMask("Player")))
        {
            Debug.Log("facing player");
            return true;
        }
        return false;
    }      

    private bool LineOfSightCheck()
    {
        Vector3 direction = transform.position - _player.transform.position;

        RaycastHit hit;
        if (Physics.Raycast(transform.position, direction, out hit, direction.magnitude, LayerMask.GetMask("Player")))
        {
            return false;
        }
        else
            return true;
    }

    public void StandYourGround()
    {
        _agent.Stop();
    }

    private void Patroling()
    {
        _agent.Resume();
        if (!_walkPointSet) SearchWalkPoint();

        if (_walkPointSet)
            _agent.SetDestination(_walkPoint);

        Vector3 distanceToWalkPoint = transform.position - _walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            _walkPointSet = false;
    }
    private void SearchWalkPoint()
    {
        //Calculate random point in range
        float randomZ = Random.Range(-_walkPointRange, _walkPointRange);
        float randomX = Random.Range(-_walkPointRange, _walkPointRange);

        _walkPoint = new Vector3(_startPatrollpoint.x + randomX, _startPatrollpoint.y, _startPatrollpoint.z + randomZ);

        if (Physics.Raycast(_walkPoint, -transform.up, 2f, _whatIsGround))
            _walkPointSet = true;
    }

    private void ChasePlayer()
    {
        _agent.Resume();
        _agent.SetDestination(_player.transform.position);
        Vector3 PlayerLocation = _player.transform.position;
        PlayerLocation.y = 0.0f;

        

        //transform.LookAt(PlayerLocation);
    }

    public void EnemyDeath()
    {
        _agent.Stop();           
        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, _sightRange);
    }

    public void Hit(int damage)
    {
        turnRed = true;
        health -= damage;
        _myMeshRenderer.material = _hitMaterial;
    }
}
