using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TheLiquidFire.Animation;
using UnityEngine.AI;

public class Elisabethbehaviour : MonoBehaviour
{

    [Header("Go's")]
    [SerializeField]
    private NavMeshAgent _agent;
    private Transform _player;

    [Header("Layermasks")]
    [SerializeField]
    private LayerMask _whatIsGround, _whatIsPlayer;

    //Patroling
    private Vector3 _walkPoint;
    private bool _walkPointSet;
    [SerializeField]
    [Range(1, 15)]
    private float _walkPointRange;

    [Header("What is elisabeth doing")]
    public ElisabethState _elisabethState;    

    [Header("Atomate behavior")]
    public bool Automate = true;
    [SerializeField]
    [Range(0, 100)]
    private float _minTimeBehaviourswitch;
    [SerializeField]
    [Range(0, 100)]
    private float _maxTimeBehaviourswitch;
    private float behaviourSwitchTimer;
    private float switchtime;

    [SerializeField]
    private Animator _elisabethAnimator;

    private Vector3 _startPatrollpoint;

    [HideInInspector]
    public Vector3 MoveToPosition;


    [Header("Elisabet Voice Automated")]
    private float _elisabethCrytimer;
    [SerializeField]
    private bool automatevoice = false;
    [SerializeField]
    [Range(0, 60)]
    private float _minElisabethVoicelineTimer;
    [SerializeField]
    [Range(0, 120)]
    private float _maxElisabethVoicelineTimer;
    private float _voiceSwitchTime;

    private AudioSource _myaudiosource;

    [Header("Elisabeth Voice clips")]
    [SerializeField]
    private List<AudioClip> _elisabewthCries = new List<AudioClip>();


    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player").transform;
        _agent = GetComponent<NavMeshAgent>();
        _myaudiosource = GetComponent<AudioSource>();
        switchtime = Random.Range(_minTimeBehaviourswitch, _maxTimeBehaviourswitch);
        _voiceSwitchTime = Random.Range(_minElisabethVoicelineTimer, _maxElisabethVoicelineTimer);
        StartPatrolling();
        _startPatrollpoint = transform.position;

        //tweener = transform.MoveToLocal(new Vector3(0, 3, 0), 1f, EasingEquations.EaseInBounce);
        //tweener.loopCount = -1;
        //tweener.loopType = EasingControl.LoopType.PingPong;
        //tweener.startValue = 0;        
    }

    void Update()
    {
        if (Automate)
        {
            ChangeBehaviour();
        }

        if (automatevoice)
        {
            playrandomvoice();
        }

        StateBehaviour();
    }

    private void playrandomvoice()
    {
        _elisabethCrytimer += Time.deltaTime;

        if (_voiceSwitchTime < _elisabethCrytimer)
        {
            _elisabethCrytimer = 0;
            _voiceSwitchTime = Random.Range(_minElisabethVoicelineTimer, _maxElisabethVoicelineTimer);

            AudioClip cliptoplay = _elisabewthCries[Random.Range(0, _elisabewthCries.Count - 1)];
            _myaudiosource.PlayOneShot(cliptoplay);
        }
    }

    public void PlayRandomSound()
    {

        AudioClip cliptoplay = _elisabewthCries[Random.Range(0, _elisabewthCries.Count - 1)];
        _myaudiosource.PlayOneShot(cliptoplay);
    }

    public void StartJumping()
    {
        _agent.Stop();
        _elisabethState = ElisabethState.jumpingInPlace;
        _elisabethAnimator.SetBool("Jumping", true);
              
    }

    public void StartPatrolling()
    {
        _agent.Stop();
        _elisabethState = ElisabethState.patrolling;
        _elisabethAnimator.SetBool("Jumping", false);        
    }
    public void StartFollowing()
    {
        _agent.Stop();
        _elisabethState = ElisabethState.following;
        _elisabethAnimator.SetBool("Jumping", false);        
    }

    public void StartWalking()
    {
        _agent.Stop();
        _elisabethState = ElisabethState.WalkTowards;
        _elisabethAnimator.SetBool("Jumping", false);        
    }

    public void ChangePatrollCenterPoint()
    {
        _startPatrollpoint = transform.position;
    }

    private void ChangeBehaviour()
    {
        behaviourSwitchTimer += Time.deltaTime;

        if (switchtime < behaviourSwitchTimer)
        {
            behaviourSwitchTimer = 0;
            switchtime = Random.Range(_minTimeBehaviourswitch, _maxTimeBehaviourswitch);

            if (_elisabethState == ElisabethState.patrolling)
            {
                StartJumping();
            }
            else if (_elisabethState == ElisabethState.jumpingInPlace)
            {
                StartPatrolling();
            }
        }       

    }

    private void StateBehaviour()
    {
        if (_elisabethState == ElisabethState.following)
        {            
           ChasePlayer();      

        }

        if (_elisabethState == ElisabethState.patrolling)
        {
            Patroling();           
        }

        if (_elisabethState == ElisabethState.jumpingInPlace)
        {
            Jumping();
        }

        if (_elisabethState == ElisabethState.WalkTowards)
        {
            MoveTo();            
        }
    }

    private void MoveTo()
    {
        _agent.Resume();
        _agent.SetDestination(MoveToPosition);
        Debug.Log("moving");
    }

    private void Jumping()
    {
        //jump
        //call for player
    }

    private void Patroling()
    {
        _agent.Resume();
        if (!_walkPointSet) SearchWalkPoint();

        if (_walkPointSet)
            _agent.SetDestination(_walkPoint);

        Vector3 distanceToWalkPoint = transform.position - _walkPoint;
        
        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
        { 
            _walkPointSet = false;
           
        }
    }

    private void SearchWalkPoint()
    {
        
        //Calculate random point in range
        float randomZ = Random.Range(-_walkPointRange, _walkPointRange);
        float randomX = Random.Range(-_walkPointRange, _walkPointRange);

        _walkPoint = new Vector3(_startPatrollpoint.x + randomX, _startPatrollpoint.y, _startPatrollpoint.z + randomZ);

        if (Physics.Raycast(_walkPoint, -transform.up, 2f, _whatIsGround))
            _walkPointSet = true;
    }

    private void ChasePlayer()
    {
        //change stopping distance in navmesh component

        _agent.Resume();
        _agent.SetDestination(_player.position);
        Vector3 PlayerLocation = _player.position;
        PlayerLocation.y = 0.0f;

        //transform.LookAt(PlayerLocation);       

    }
}

public enum ElisabethState
{
    following,
    patrolling,
    jumpingInPlace,
    WalkTowards
}
