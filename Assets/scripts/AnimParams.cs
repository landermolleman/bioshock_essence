using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AnimParams
{
    public Vector3 Endpos;
    public float Time;
    public GameObject ObjectToAnim;
}

[System.Serializable]
public class SingleAnimation
{
    public List<AnimParams> AnimParams;
}

[System.Serializable]
public class Enemyspawner
{
    public List<EnemyParams> EnemyParams;
}

[System.Serializable]
public class EnemyParams
{
    public bool SpawnEnemy;
    //public Vector3 SpawnPos;
    public GameObject SpawnPos;
}
