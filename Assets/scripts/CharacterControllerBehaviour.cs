﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CharacterController))]
public class CharacterControllerBehaviour : MonoBehaviour
{
    private CharacterController _characterController;
    private Vector3 _velocity = Vector3.zero;
    private Vector3 _jumpforce = Vector3.zero;
    private Vector3 _direction;
    private Vector3 _groundMovement;
    private Quaternion _rotAngle;

    [Header("Gameobject references")]
    [SerializeField]
    public Transform cam;
    [SerializeField]
    private Animator _hand;

    [Header("Gun plugin")]
    [SerializeField]
    private GunBase _gun;

    readonly float _turnSmoothTime = 0.1f;
    private float _turnSmoothVelocity;
    private bool _jump;

    [Header("Movement")]
    [SerializeField]
    [Range(2, 20)]
    private float _moveSpeed;
    [SerializeField]
    [Range(1, 5)]
    private float _jumpHeight;
    [SerializeField]
    [Range(1, 100)]
    private float _dragOnGround;

    [Header("Mouse & looking")]
    [SerializeField]
    [Range(10, 500)]
    private float _mouseSensitivity = 180f;
    [SerializeField]
    private Vector2 rotationYLimit = new Vector2(-20, 20);
    private Vector2 mouseRotation;
    private float _horizontal;
    private float _vertical;

    [Header("Health")]
    [SerializeField]
    [Range(1, 50)]
    public int health = 10;
    private float timeSincelastShot;

   


    void Start()
    {
        _characterController = GetComponent<CharacterController>();        
        _jumpforce = -Physics.gravity.normalized * Mathf.Sqrt(2 * Physics.gravity.magnitude * _jumpHeight);
        Cursor.lockState = CursorLockMode.Locked;
        

    }

    private void FixedUpdate()
    {
        ApplyGround();
        ApplyGravity();        
        ApplyMovement();
        ApplyGroundDrag();      
        ApplyJump();        

        _characterController.Move((_groundMovement * Time.deltaTime) + (_velocity * Time.deltaTime));


    }
    void Update()
    {      
        MovementInput();

        shoot();

        if (health <= 0)
        {
            PlayerDeath();
        }      
    }

 

    private void ApplyJump()
    {
        

        if (_jump && _characterController.isGrounded)
        {
            _velocity += _jumpforce;
            _jump = false;
        }

    }
   

    private void ApplyGroundDrag()
    {
        if (_characterController.isGrounded)
        {
            _groundMovement = _groundMovement * (1 - Time.deltaTime * _dragOnGround);
        }
    }
  

    private void ApplyMovement()
    {
        if (_characterController.isGrounded)
        {
            if (_direction.magnitude >= 0.1f)
            {
                _hand.SetBool("isRunning", true);

                //aplly thridpersonmovement and adding it up with the cameras Y rotation.
                float targetangle = Mathf.Atan2(_direction.x, _direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetangle, ref _turnSmoothVelocity, _turnSmoothTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                //move forward in current forwardirection
                Vector3 moveDirection = Quaternion.Euler(0f, targetangle, 0f) * Vector3.forward;
                _groundMovement = moveDirection.normalized * _moveSpeed;

                ////set global variable for transition to swimming and/or flying
                //_rotAngle = Quaternion.Euler(0f, angle, 0f);
            }
            else
                _hand.SetBool("isRunning", false);
        }

    }


    private void shoot()
    {
        timeSincelastShot += Time.deltaTime;
        if (Input.GetButtonDown("Fire1") && _gun.firerate < timeSincelastShot)
        {
            _gun.Shoot(true, null);
            timeSincelastShot = 0;
        }
    }

    private void MovementInput()
    {
        _horizontal = UnityEngine.Input.GetAxisRaw("Horizontal");
        _vertical = UnityEngine.Input.GetAxisRaw("Vertical");
        _direction = new Vector3(_horizontal, 0f, _vertical).normalized;

        if (Input.GetButtonDown("Jump"))
        {
            _jump = true;
        }

        mouseRotation.y += Input.GetAxis("Mouse X") * _mouseSensitivity * Time.deltaTime;
        mouseRotation.x += -Input.GetAxis("Mouse Y") * _mouseSensitivity * Time.deltaTime;

        mouseRotation.x = Mathf.Clamp(mouseRotation.x, rotationYLimit.x, rotationYLimit.y);

        cam.transform.rotation = Quaternion.Euler(mouseRotation.x, mouseRotation.y, 0);
    }

    private void ApplyGround()
    {
        if (_characterController.isGrounded)
        {
            _velocity -= Vector3.Project(_velocity, Physics.gravity.normalized);

        }
    }

    private void ApplyGravity()
    {
        if (!_characterController.isGrounded)
        {
            _velocity += Physics.gravity * 2.5f * Time.fixedDeltaTime; // g[m/s^2] * t[s]
        }
    }

    public void Hit(int damage)
    {        
        health -= damage;        
    }

    public void PlayerDeath()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
