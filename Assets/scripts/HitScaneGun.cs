using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitScaneGun : GunBase
{
    private void Start()
    {
        _myAnimation = GetComponent<Animator>();
    }
    public override void Shoot(bool isPlayer, GameObject player)
    {
        _myAnimation.SetTrigger("shoot");

        RaycastHit hit;
        if (isPlayer)
        {
            if (Physics.Raycast(GunPoint.transform.position, GunPoint.forward, out hit, 100, LayerMask.GetMask("Enemy")))
            {
                

                hit.transform.gameObject.GetComponentInParent<enemyAI>().Hit(gunDamage);
            }
        }
        else
        {          

            int hitOrNot = Random.Range(0, 3);

            if (hitOrNot == 0)
            {
                player.gameObject.GetComponentInParent<CharacterControllerBehaviour>().Hit(gunDamage);
            }
            
        }

    }
}

    
    