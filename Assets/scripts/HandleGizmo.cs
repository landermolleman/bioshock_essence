using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleGizmo : MonoBehaviour
{
   

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, 1);
    }
}
