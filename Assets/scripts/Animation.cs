using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation : MonoBehaviour
{
    [SerializeField]
    private Vector3 _endposition;
    private Vector3 _startposition;

    [SerializeField]
    private float _speed;

    public void Initialize(Vector3 endposition, float duration)
    {
        _endposition = endposition;
        this._speed = duration;
        
    }

    private void Start()
    {
        _startposition = transform.position;
    }

    private void Update()
    {
        Animate();
    }

    private void Animate()
    {
        transform.position = Vector3.Lerp(transform.position, _startposition + _endposition, Time.deltaTime * _speed);

        if ((transform.position - (_startposition + _endposition)).magnitude < 0.1f)
        {
            Destroy(this);
        }
    }
}
