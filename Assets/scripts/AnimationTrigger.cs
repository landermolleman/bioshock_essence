using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTrigger : MonoBehaviour
{
    [Header("Amount this hitbox will be triggered")]
    [HideInInspector]
    public bool triggered = false;
    [SerializeField]
    public int Triggeramount = 1;    
    private int _triggercounter = 1;   
    
    [Header("Elisabeth (destination for walking to)")]
    [SerializeField]
    Elisabethbehaviour ElisabethRef;
    public List<ElisabethState> _elisabethTargetState;
    public List<GameObject> ElisabethDestination = new List<GameObject>();

    [Header("ObjectsToAnimate")]   
    public List<SingleAnimation> Animations = new List<SingleAnimation>();

    [Header("EnemiesToSpawn")]
    public GameObject EnemyPrefab = null;
    public List<Enemyspawner> EnemySpawnList = new List<Enemyspawner>();

    [Header("Narration")]
    [SerializeField]
    private List<AudioClip> _narrationList = new List<AudioClip>();
    private AudioSource _myaudiosource;



    private void Awake()
    {
        _myaudiosource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (_triggercounter >= Triggeramount)
        {
            triggered = true;
        }       

        if (Animations.Count >= 0)
        {
            AnimationTriggeractivate();
        }

        if (ElisabethRef != null)
        {
            elisabethTrigger();
        }

        if (_narrationList != null)
        {
            PlayNarration();
        }

        if (EnemySpawnList != null)
        {
            SpawnEnemies();
        }

        _triggercounter++;
    }

    private void elisabethTrigger()
    {
        if (_triggercounter <= _elisabethTargetState.Count)
        {
            if (_elisabethTargetState[_triggercounter - 1] == ElisabethState.following)
            {
                ElisabethRef.StartFollowing();
                Debug.Log("following");
            }

            if (_elisabethTargetState[_triggercounter - 1] == ElisabethState.patrolling)
            {
                ElisabethRef.StartPatrolling();
            }

            if (_elisabethTargetState[_triggercounter - 1] == ElisabethState.jumpingInPlace)
            {
                ElisabethRef.StartJumping();
            }

            if (_elisabethTargetState[_triggercounter - 1] == ElisabethState.WalkTowards)
            {
                ElisabethRef.MoveToPosition = ElisabethDestination[_triggercounter - 1].transform.position;
                ElisabethRef.StartWalking();
            }
        }
    }

    private void SpawnEnemies()
    {
        if (_triggercounter <= EnemySpawnList.Count)
        {
            for (int i = 0; i < EnemySpawnList[_triggercounter - 1].EnemyParams.Count; i++)
            {
                if (EnemySpawnList[_triggercounter - 1].EnemyParams[i].SpawnEnemy)
                {
                    Instantiate(EnemyPrefab, EnemySpawnList[_triggercounter - 1].EnemyParams[i].SpawnPos.transform.position , Quaternion.identity);
                }
            }
        }
    }

    private void PlayNarration()
    {
        if (_triggercounter <= _narrationList.Count)
        {
            _myaudiosource.PlayOneShot(_narrationList[_triggercounter - 1]);
        }
    }

    private void AnimationTriggeractivate()
    {
        if (_triggercounter <= Animations.Count)
        {
            for (int i = 0; i < Animations[_triggercounter -1].AnimParams.Count; i++)
            {
                Animations[_triggercounter -1].AnimParams[i].ObjectToAnim.AddComponent<Animation>();
                Animation animation = Animations[_triggercounter -1 ].AnimParams[i].ObjectToAnim.GetComponent<Animation>();
                animation.Initialize(Animations[_triggercounter -1].AnimParams[i].Endpos, Animations[_triggercounter -1].AnimParams[i].Time);                       
            }
        }
    }

   
}
