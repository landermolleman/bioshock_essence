﻿using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Animation;
using UnityEngine;

public class AnimationTest : MonoBehaviour
{



    Tweener tweener = null;
    public bool stopped = false;

    void Start()
    {
        //possible functions = MoveTo, MoveToLocal, ScaleTo, RotateTo, RotateToLocal
        //change animationtype by adjusting "EasingEquations.___"
        
        tweener = transform.MoveToLocal(new Vector3(0, 5, 0), 1f, EasingEquations.EaseInOutCubic);
        //Tweener tweener = transform.RotateTo(Quaternion.Euler(180, 0, 0), 1f, EasingEquations.Linear);

        // make loop infinite = -1
        tweener.loopCount = -1;
        //loopType
        tweener.loopType = EasingControl.LoopType.PingPong;
        //0= start currentl ocation, -1 = start vector 3(-x,-y,-z)
        tweener.startValue = 0;          
    }

    private void Update()
    {
        // callable methods to influnce the animation after initialization
        //reverse, pause, resume, stop,..

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (stopped)
            {
                tweener.Pause();
                stopped = false;
            }
            else
            {
                tweener.Resume();
                stopped = true;
            }                
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            tweener.Stop();
            
            
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            tweener.Play();
        }

    }
}
