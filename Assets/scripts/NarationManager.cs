using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NarationManager : MonoBehaviour
{
    [SerializeField]
    private List<AnimationTrigger> _animationTriggerBoxList = new List<AnimationTrigger>();

    public int ActiveAnimation = 0;

    private void Start()
    {
        foreach (var ATB in _animationTriggerBoxList)
        {
            ATB.gameObject.SetActive(false);
        }
        _animationTriggerBoxList[0].gameObject.SetActive(true);
    }


    private void Update()
    {
        if (_animationTriggerBoxList[ActiveAnimation].triggered)
        {
            if (ActiveAnimation + 1 < _animationTriggerBoxList.Count)
            {
                _animationTriggerBoxList[ActiveAnimation + 1].gameObject.SetActive(true);
            }

            _animationTriggerBoxList[ActiveAnimation].gameObject.SetActive(false);
            ActiveAnimation++;
        }
    }

}
